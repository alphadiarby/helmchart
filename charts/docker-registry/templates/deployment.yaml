apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ include "docker-registry.fullname" . }}
  labels:
    app: {{ template "docker-registry.name" . }}
    release: {{ .Release.Name }}
spec:
  selector:
    matchLabels:
      app: {{ template "docker-registry.name" . }}
      release: {{ .Release.Name }}
  replicas: {{ .Values.replicaCount }}
{{- if .Values.updateStrategy }}
  strategy:
{{ toYaml .Values.updateStrategy | indent 4 }}
{{- end }}
  template:
    metadata:
      {{- with .Values.podAnnotations }}
      annotations:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      labels:
        app: {{ template "docker-registry.name" . }}
        release: {{ .Release.Name }}
    spec:
      {{- if .Values.imagePullSecrets }}
      imagePullSecrets:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      securityContext:
        {{- toYaml .Values.podSecurityContext | nindent 8 }}
      containers:
        - name: {{ .Chart.Name }}
          securityContext:
            {{- toYaml .Values.securityContext | nindent 12 }}
          image: "{{ .Values.image.repository }}:{{ .Values.image.tag }}"
          imagePullPolicy: {{ .Values.image.pullPolicy }}
          ports:
            - name: http
              containerPort: 5000
              protocol: TCP
          livenessProbe:
            httpGet:
              path: /
              port: http
          readinessProbe:
            httpGet:
              path: /
              port: http
          resources:
            {{- toYaml .Values.resources | nindent 12 }}
          volumeMounts:
          - name: repo-vol
            mountPath: "/var/lib/registry"
          {{- if .Values.tlsSecretName }}
          - name: certs-vol
            mountPath: "/certs"
            readOnly: true
          {{- end }}
          {{ if .Values.secrets.htpasswd }}
          - name: auth
            mountPath: "/auth"
            readOnly: true
          {{- end }}
{{- with .Values.extraVolumeMounts }}
            {{- toYaml . | nindent 12 }}
{{- end }}
          env:
          - name: REGISTRY_AUTH
            value: "htpasswd"
          - name: REGISTRY_AUTH_HTPASSWD_REALM
            value: "Registry Realm"
          - name: REGISTRY_AUTH_HTPASSWD_PATH
            value: "/auth/htpasswd"
          - name: REGISTRY_HTTP_TLS_CERTIFICATE
            value: "/certs/tls.crt"
          - name: REGISTRY_HTTP_TLS_KEY
            value: "/certs/tls.key"
{{- with .Values.extraVars }}
        {{- toYaml . | nindent 12 }}
{{- end }}
      volumes:
      - name: repo-vol
      {{- if .Values.persistence.enabled }}
        persistentVolumeClaim:
          claimName: {{ if .Values.persistence.existingClaim }}{{ .Values.persistence.existingClaim }}{{- else }}{{ template "docker-registry.fullname" . }}{{- end }}
       {{- end }}
  {{- if .Values.tlsSecretName }}
      - name: certs-vol
        secret:
          secretName: {{ .Values.tlsSecretName }}
      - name: auth
        secret:
          secretName: {{ template "docker-registry.fullname" . }}-secret
          items:
            - key: htpasswd
              path: htpasswd
  {{- end }}
{{- with .Values.extraVolumes }}
      {{- toYaml . | nindent 8 }}
{{- end }} 
